﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace efgetstarted
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using (var db = new BloggingContext())
            {
                /*
                Console.WriteLine("Inserindo blogs");
                await db.AddAsync(new Blog{Url="http://blogs.msdn.com/adonet"});
                await db.AddAsync(new Blog{Url="https://devblogs.microsoft.com/dotnet"});
                await db.SaveChangesAsync();
                */
                /*
                Console.WriteLine("Consultando todos os blogs");
                var blogs = db.Blogs.OrderBy(b => b.BlogId);
                blogs.ToList().ForEach(b => Console.WriteLine($"{b.BlogId} - {b.Url}"));
                Console.WriteLine("Consultando um blog");
                var blog = await db.Blogs.FindAsync(2);
                Console.WriteLine($"{blog.BlogId} - {blog.Url}");

                Console.WriteLine("Alterando dados");
                blog.Posts.Add(new Post{Title="Alô Mundo!", Content="Teste de alteração"});
                await db.SaveChangesAsync();
                */
                /*
                Console.WriteLine("Consultando eager todos os blogs com seus posts");
                var blogs = db.Blogs.Include(b => b.Posts).OrderBy(b => b.BlogId);

                blogs.ToList().ForEach(b => 
                {
                    Console.WriteLine($"{b.BlogId} - {b.Url}");
                    b.Posts.ForEach(p => Console.WriteLine($"{p.Title}"));
                });
                */
                /*
                Console.WriteLine("Consultando explicit todos os blogs com seus posts");
                var blogs = db.Blogs.OrderBy(b => b.BlogId);

                blogs.ToList().ForEach(async b => 
                {
                    Console.WriteLine($"{b.BlogId} - {b.Url}");
                    await db.Entry(b).Collection(oblog => oblog.Posts).LoadAsync();
                    b.Posts.ForEach(p => Console.WriteLine($"{p.Title}"));
                });
                */

                Console.WriteLine("Removendo um blog");
                var blog = await db.Blogs.FindAsync(2);
                db.Remove(blog);
                await db.SaveChangesAsync();
            }
        }
    }
}
